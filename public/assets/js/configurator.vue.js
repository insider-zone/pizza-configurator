Vue.component('available-topping', {
	props: ['topping', 'currency'],
	template: 
	'<span class="badge badge-primary">{{topping.name}} \
		(<span v-for="price in topping.price" v-if="price.symbol == currency">{{price.value}} {{price.symbol}}</span>)\
	<i class="fas fa-plus-square" v-on:click="add(topping.id)"></i></span>',
	methods: {
		add(id) {
			pizzaConfiguratorApp.selectTopping(id);
		}
	}
});

Vue.component('selected-topping', {
	props: ['topping', 'currency'],
	template: 
	'<span class="badge badge-success">{{topping.name}} \
		(<span v-for="price in topping.price" v-if="price.symbol == currency">{{price.value}} {{price.symbol}}</span>)\
	<i class="fas fa-minus-square" v-on:click="remove(topping.id)"></i></span>',
	methods: {
		remove(id) {
			pizzaConfiguratorApp.unSelectTopping(id);
		}
	}
});

Vue.component('currency-selector', {
	props: ['currencies', 'selected'],
	template: 
	'<div class="dropdown">\
		<button class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false" type="button">{{selected}} </button>\
		<div class="dropdown-menu dropdown-menu-left" role="menu">\
			<span v-for="currency in currencies" v-bind:currencies="currencies" v-bind:key="currency.symbol" v-on:click="selectCurrency(currency.symbol)" class="dropdown-item" role="presentation">{{currency.symbol}} ({{currency.displayName}})</span>\
		</div>\
	</div>',
	methods: {
		selectCurrency(symbol) {
			pizzaConfiguratorApp.selectCurrency(symbol);
		}
	}
});

var pizzaConfiguratorApp = new Vue({
	el: '#pizza-configurator-app',
	data: {
		availableToppings: [],
		selectedToppings: [],
		totalValue: {},
		currencies: [],
		selectedCurrencySymbol: '',
		minimumToppingCount: 3,
		maximumToppingCount: 7,
		isMinimumToppingCountReached: false,
		roundFactor: 1,
	},
	mounted: function() {
		this.asyncLoadAppData();
	},
	methods: {
		asyncLoadAppData() {
			this.$http.get('/data').then(response => {
				this.updateInstanceData(response.body);
			}, response => {
				alert('Seems like the application server is not reachable :|');
			});
		},
		updateInstanceData(data) {
			this.currencies = data.currencies;
			this.selectedCurrencySymbol = data.currencies[0].symbol;
			for(var i = 0; i < data.currencies.length; i++) {
				this.totalValue[data.currencies[i].symbol] = 0;
			}
			this.availableToppings = data.toppings;
			this.removeLoaderSheet();
		},
		removeLoaderSheet() {
			setTimeout(function(){
				document.getElementById('loader').remove();
			}, 350);
		},
		selectCurrency(symbol) {
			this.selectedCurrencySymbol = symbol;
			this.calculateRoundFactor();
		},
		calculateRoundFactor() {
			for(var i = 0; i < this.currencies.length; i++) {
				if(this.currencies[i].symbol == this.selectedCurrencySymbol) {
					this.roundFactor = Math.pow(10, this.currencies[i].decimals);
					break;
				}
			}
		},
		selectTopping(id) {
			if(this.selectedToppings.length == this.maximumToppingCount) {
				alert(
					"You can not add more than " +
					this.maximumToppingCount +
					" toppings to your pizza :("
				);
				return;
			}
			
			for(var i = 0; i < this.availableToppings.length; i++) {
				if(this.availableToppings[i].id == id) {
					this.selectedToppings.push(this.availableToppings[i]);
					this.availableToppings.splice(i, 1);
					break;
				}
			}
			this.updateTotal();
			this.checkMinimumToppingCount();
		},
		unSelectTopping(id) {
			for(var i = 0; i < this.selectedToppings.length; i++) {
				if(this.selectedToppings[i].id == id) {
					this.availableToppings.push(this.selectedToppings[i]);
					this.selectedToppings.splice(i, 1);
					break;
				}
			}
			this.updateTotal();
			this.checkMinimumToppingCount();
		},
		updateTotal() {
			var keys = Object.keys(this.totalValue);
			for(var i = 0; i < keys.length; i++) {
				this.totalValue[keys[i]] = 0;
			}
			
			for(var i = 0; i < this.selectedToppings.length; i++) {
				for(var j = 0; j < this.selectedToppings[i].price.length; j++) {
					this.totalValue[this.selectedToppings[i].price[j].symbol] += this.selectedToppings[i].price[j].value;
				}
			}
		},
		checkMinimumToppingCount() {
			if(this.selectedToppings.length >= this.minimumToppingCount) {
				this.isMinimumToppingCountReached = true;
			}
			else {
				this.isMinimumToppingCountReached = false;
			}
		}
	}
});
