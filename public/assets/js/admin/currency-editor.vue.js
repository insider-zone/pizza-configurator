Vue.component('currency-block', {
	props: ['currency'],
	template: 
	'<div class="row">\
		<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">\
			<div class="row record">\
				<div class="col">\
					<form v-on:submit="saveCurrencyData">\
						<div class="form-row">\
							<div class="col-10 col-sm-10 col-md-11 col-lg-11 col-xl-12">\
								<div class="form-row">\
									<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">\
										<div class="form-group"><label><strong>Symbol</strong> of the currency (3 capital letter code):</label><input v-model.lazy="currency.symbol" class="form-control" type="text" pattern="[A-Z]{3}" maxlength="3" required></div>\
									</div>\
									<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">\
										<div class="form-group"><label><strong>Displayed name</strong> of the currency:</label><input v-model.lazy="currency.displayName" class="form-control" type="text" maxlength="64" required></div>\
									</div>\
									<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">\
										<div class="form-group"><label><strong>Decimals</strong> of the prices when using this currency:</label><input v-model.lazy="currency.decimals" class="form-control" type="number" min="0" max="8" step="1" required></div>\
									</div>\
									<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">\
										<div class="btn-group float-right" role="group"><button v-on:click="confirmRemoval()" class="btn btn-danger" type="button">Delete</button><button class="btn btn-success" type="submit">Save</button></div>\
									</div>\
								</div>\
							</div>\
						</div>\
					</form>\
				</div>\
			</div>\
			<div class="row">\
				<div class="col">\
					<hr>\
				</div>\
			</div>\
		</div>\
	</div>',
	methods: {
		confirmRemoval() {
			var question = "Are you sure you want to delete "+this.currency.displayName+" ("+this.currency.symbol+")?";
			if(this.currency.displayName.length < 1 || this.currency.symbol.length < 3) {
				question = "Are you sure you want to delete this element?";
			}
			
			if(confirm(question) == true) {
				this.removeCurrency();
			}
		},
		removeCurrency() {
			if(this.currency.id < 0) {
				currencyEditorApp.removeCurrencyComponent(this.currency.id);
			}
			else {
				this.$http.post('/admin/currencies/remove', {
					id: this.currency.id
				}).then(response => {
					currencyEditorApp.removeCurrencyComponent(this.currency.id);
				}, response => {
					alert('An error occured. Failed to remove currency.');
				});
			}
		},
		saveCurrencyData(e) {
			e.preventDefault();
			this.$http.post('/admin/currencies/save', {
				id: this.currency.id,
				symbol: this.currency.symbol,
				displayName: this.currency.displayName,
				decimals: this.currency.decimals
			}).then(response => {
				this.currency.id = response.body.id;
				alert('OK it\'s saved.')
			}, response => {
				alert('An error occured. Failed to save currency.');
			});
		}
	}
});

var currencyEditorApp = new Vue({
	el: '#currency-editor-app',
	data: {
		currencies: [],
	},
	mounted: function() {
		this.asyncLoadAppData();
		Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name=csrf-token]').attr('content');
	},
	methods: {
		asyncLoadAppData() {
			this.$http.get('/admin/currencies/data').then(response => {
				this.updateInstanceData(response.body);
			}, response => {
				alert('Seems like the application server is not reachable :|');
			});
		},
		updateInstanceData(data) {
			this.currencies = data;
			this.removeLoaderSheet();
		},
		removeLoaderSheet() {
			setTimeout(function(){
				document.getElementById('loader').remove();
			}, 400);
		},
		addCurrencyComponent() {
			this.currencies.push({
				id: -1 * (new Date() / 1000),
				symbol: '',
				displayName: '',
				decimals: 0
			});
		},
		removeCurrencyComponent(id) {
			for(var i = 0; i < this.currencies.length; i++) {
				if(this.currencies[i].id == id) {
					this.currencies.splice(i, 1);
					alert('OK it\'s removed.');
					break;
				}
			}
		}
	}
});
