Vue.component('component-block', {
	props: ['component'],
	template: 
	'<div class="row">\
		<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">\
			<div class="row record">\
				<div class="col">\
					<form v-on:submit="saveComponentData">\
						<div class="form-row">\
							<div class="col-10 col-sm-10 col-md-11 col-lg-11 col-xl-12">\
								<div class="form-row">\
									<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">\
										<div class="form-group"><label><strong>Name</strong> of the component:</label><input v-model.lazy="component.name" class="form-control" type="text" maxlength="32" required></div>\
									</div>\
									<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"><span><strong>Pricing</strong> of the component:</span>\
										<div class="form-row pricing">\
											<div class="col">\
												<div class="form-row">\
													<div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1"></div>\
													<div class="col-11 col-sm-11 col-md-11 col-lg-11 col-xl-11">\
														<div class="form-row" v-for="price in component.price" v-bind:key="price.symbol">\
															<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">\
																<div class="form-group"><label><strong>{{price.symbol}}</strong></label><input v-model.lazy="price.value" class="form-control" type="number" min="0" step="0.01" required></div>\
															</div>\
														</div>\
													</div>\
												</div>\
											</div>\
										</div>\
									</div>\
									<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">\
										<div class="btn-group float-right" role="group"><button class="btn btn-danger" v-on:click="confirmRemoval()" type="button">Delete</button><button class="btn btn-success" type="submit">Save</button></div>\
									</div>\
								</div>\
							</div>\
						</div>\
					</form>\
				</div>\
			</div>\
			<div class="row">\
				<div class="col">\
					<hr>\
				</div>\
			</div>\
		</div>\
	</div>',
	methods: {
		confirmRemoval() {
			var question = "Are you sure you want to delete "+this.component.name+"?";
			if(this.component.name.length < 1) {
				question = "Are you sure you want to delete this element?";
			}
			
			if(confirm(question) == true) {
				this.removeComponent();
			}
		},
		removeComponent() {
			if(this.component.id < 0) {
				componentEditorApp.removeComponent(this.component.id);
			}
			else {
				this.$http.post('/admin/components/remove', {
					id: this.component.id
				}).then(response => {
					componentEditorApp.removeComponent(this.component.id);
				}, response => {
					alert('An error occured. Failed to remove component.');
				});
			}
		},
		saveComponentData(e) {
			e.preventDefault();
			this.$http.post('/admin/components/save', {
				id: this.component.id,
				name: this.component.name,
				price: JSON.stringify(this.component.price)
			}).then(response => {
				this.component.id = response.body.id;
				alert('OK it\'s saved.')
			}, response => {
				alert('An error occured. Failed to save component.');
			});
		}
	}
});

var componentEditorApp = new Vue({
	el: '#component-editor-app',
	data: {
		currencies: [],
		components: [],
	},
	mounted: function() {
		this.asyncLoadAppData();
		Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name=csrf-token]').attr('content');
	},
	methods: {
		asyncLoadAppData() {
			this.$http.get('/admin/components/data').then(response => {
				this.updateInstanceData(response.body);
			}, response => {
				alert('Seems like the application server is not reachable :|');
			});
		},
		updateInstanceData(data) {
			this.currencies = data.currencies;
			this.components = data.toppings;
			this.removeLoaderSheet();
		},
		removeLoaderSheet() {
			setTimeout(function(){
				document.getElementById('loader').remove();
			}, 400);
		},
		addComponent() {
			this.components.push({
				id: -1 * (new Date() / 1000),
				name: '',
				price: this.getEmptyPriceData()
			});
		},
		getEmptyPriceData() {
			var data = [];
			for(var i = 0; i < this.currencies.length; i++) {
				data.push({
					symbol: this.currencies[i].symbol,
					value: 0,
				});
			}
			return data;
		},
		removeComponent(id) {
			for(var i = 0; i < this.components.length; i++) {
				if(this.components[i].id == id) {
					this.components.splice(i, 1);
					alert('OK it\'s removed.');
					break;
				}
			}
		}
	}
});
