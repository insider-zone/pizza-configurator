<?php

use Illuminate\Database\Seeder;

class PizzaComponentSeeder extends Seeder
{	
	const TABLE = 'pizza_components';
	
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$component = new \App\Models\PizzaComponent();
		$component->id = 1;
		$component->name = "Ham";
		$component->save();
		
		$component = new \App\Models\PizzaComponent();
		$component->id = 2;
		$component->name = "Onion";
		$component->save();
		
		$component = new \App\Models\PizzaComponent();
		$component->id = 3;
		$component->name = "Salami";
		$component->save();
		
		$component = new \App\Models\PizzaComponent();
		$component->id = 4;
		$component->name = "Mozzarella";
		$component->save();
		
		$component = new \App\Models\PizzaComponent();
		$component->id = 5;
		$component->name = "Corn";
		$component->save();
		
		$component = new \App\Models\PizzaComponent();
		$component->id = 6;
		$component->name = "Ananas";
		$component->save();
		
		$component = new \App\Models\PizzaComponent();
		$component->id = 7;
		$component->name = "Bacon";
		$component->save();
		
		$component = new \App\Models\PizzaComponent();
		$component->id = 8;
		$component->name = "Salmon";
		$component->save();
		
		$component = new \App\Models\PizzaComponent();
		$component->id = 9;
		$component->name = "Olive";
		$component->save();
		
		$component = new \App\Models\PizzaComponent();
		$component->id = 10;
		$component->name = "Pepperoni";
		$component->save();
	}
}
