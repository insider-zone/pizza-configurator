<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User
        DB::table(UserSeeder::TABLE)->truncate();
        $this->call(UserSeeder::class);
        
        // PizzaComponentCurrency
        DB::table(PizzaComponentCurrencySeeder::TABLE)->truncate();
        $this->call(PizzaComponentCurrencySeeder::class);
        
        // PizzaComponent
        DB::table(PizzaComponentSeeder::TABLE)->truncate();
        $this->call(PizzaComponentSeeder::class);
        
        // PizzaComponentPrice
        DB::table(PizzaComponentPriceSeeder::TABLE)->truncate();
        $this->call(PizzaComponentPriceSeeder::class);
    }
}
