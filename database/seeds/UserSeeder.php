<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
	const TABLE = 'users';
	
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
		$user = new \App\User();
		$user->name = 'Milan Patartics';
		$user->email = 'milan@pizzaconfigurator.dev';
		$user->password = bcrypt('PizzaDev');
		$user->save();
    }
}
