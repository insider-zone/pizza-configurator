<?php

use Illuminate\Database\Seeder;

class PizzaComponentPriceSeeder extends Seeder
{	
	const TABLE = 'pizza_component_prices';
	
	private $hufMin = 100;
	private $hufMax = 300;
	
	private $usdPerHuf = 280;
	private $eurPerHuf = 315;
	
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$componentPrice = null;
		
		$components = App\Models\PizzaComponent::all();
		foreach ($components as $component){
			$randHuf = rand($this->hufMin, $this->hufMax);
			
			// HUF
			$componentPrice = new \App\Models\PizzaComponentPrice();
			$componentPrice->value = $randHuf;
			$componentPrice->pizza_component_id = $component->id;
			$componentPrice->pizza_component_currency_id = 1;
			$componentPrice->save();
			
			// USD
			$componentPrice = new \App\Models\PizzaComponentPrice();
			$componentPrice->value = $randHuf / $this->usdPerHuf;
			$componentPrice->pizza_component_id = $component->id;
			$componentPrice->pizza_component_currency_id = 2;
			$componentPrice->save();
			
			// EUR
			$componentPrice = new \App\Models\PizzaComponentPrice();
			$componentPrice->value = $randHuf / $this->eurPerHuf;
			$componentPrice->pizza_component_id = $component->id;
			$componentPrice->pizza_component_currency_id = 3;
			$componentPrice->save();
		}
	}
}
