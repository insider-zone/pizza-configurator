<?php

use Illuminate\Database\Seeder;

class PizzaComponentCurrencySeeder extends Seeder
{	
	const TABLE = 'pizza_component_currencies';
	
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$currency = new \App\Models\PizzaComponentCurrency();
		$currency->id = 1;
		$currency->symbol = "HUF";
		$currency->display_name = "Magyar Forint";
		$currency->decimals = 0;
		$currency->save();
		
		$currency = new \App\Models\PizzaComponentCurrency();
		$currency->id = 2;
		$currency->symbol = "USD";
		$currency->display_name = "USA Dollar";
		$currency->decimals = 2;
		$currency->save();
		
		$currency = new \App\Models\PizzaComponentCurrency();
		$currency->id = 3;
		$currency->symbol = "EUR";
		$currency->display_name = "Euro";
		$currency->decimals = 2;
		$currency->save();
	}
}
