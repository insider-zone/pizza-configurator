<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePizzaComponentCurrenciesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'pizza_component_currencies';

    /**
     * Run the migrations.
     * @table pizza_component_currencies
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->comment('ID');
            $table->string('symbol', 3)->comment('3 letter symbol or the currency');
            $table->text('display_name')->comment('Displayed name of the currency');
            $table->unsignedTinyInteger('decimals')->default('0')->comment('Number of decimals of the price');
            $table->timestamps();

            $table->unique(["symbol"], 'symbol_UNIQUE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
