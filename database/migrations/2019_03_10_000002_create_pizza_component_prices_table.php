<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePizzaComponentPricesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'pizza_component_prices';

    /**
     * Run the migrations.
     * @table pizza_component_prices
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->comment('ID');
            $table->double('value')->comment('Value of the pizza component price');
            $table->unsignedInteger('pizza_component_id')->comment('ID of the belonging pizza component');
            $table->unsignedInteger('pizza_component_currency_id')->comment('ID of the belonging pizza component currency');
            $table->timestamps();

            $table->index(["pizza_component_currency_id"], 'pizza_component_currency_id_INDEX');
            $table->index(["value"], 'value_INDEX');
            $table->index(["pizza_component_id"], 'pizza_component_id_INDEX');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
