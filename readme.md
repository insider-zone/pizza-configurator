# Pizza Configurator
Laravel 5.7.x + Vue JS 2.5.x

# Installation

    1, clone the repo to your local computer
    2, set the proper access rights for directories
    3, install the project using composer
    4, change the .env file to customize DB connection details
    5, run the migration files
    6, run the seeder scripts


If the migration or seed script fails, run the composer dump-autoload command!

Pizza configurator: http://pizzaconfigurator.dev/

Admin editors: http://pizzaconfigurator.dev/admin

Login details are in the UserSeeder.php file, but you can also add new users in no time.

By default the app is configured to run on localhost under the http://pizzaconfigurator.dev/ domain name, so you have to configure your webserver to have a virtualhost and you also need to edit your hosts file to redirect this domain to 127.0.0.1 or whatever local IP address you are using.
