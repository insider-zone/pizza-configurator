<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConfiguratorController extends Controller
{
    public function index()
    {
        return view('configurator');
    }
    
    public function data()
    {
		echo json_encode((object)[
			'currencies' => \App\Models\PizzaComponentCurrency::getFrontendData(),
			'toppings' => \App\Models\PizzaComponent::getFrontendData(),
		]);
    }
}
