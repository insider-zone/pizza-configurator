<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	
    public function showComponentEditor()
    {
        return view('admin.components');
    }
    
    public function showCurrencyEditor()
    {
        return view('admin.currencies');
    }
    
    public function componentData()
    {
		echo json_encode((object)[
			'currencies' => \App\Models\PizzaComponentCurrency::getFrontendData(),
			'toppings' => \App\Models\PizzaComponent::getFrontendData(),
		]);
	}
	
	public function saveComponent(Request $request)
	{
		$validation = $request->validate([
			'id' => 'required|numeric',
			'name' => 'required|string|max:32',
			'price' => 'required|string'
		]);
		
		$id = null;
		if($validation['id'] < 0) {
			$id = \App\Models\PizzaComponent::insertNew($validation);
		}
		else {
			$id = \App\Models\PizzaComponent::updateExisting($validation);
		}
		
		echo json_encode((object)[
			'id' => $id,
		]);
	}
	
	public function removeComponent(Request $request)
	{
		$validation = $request->validate([
			'id' => 'required|exists:pizza_components,id'
		]);
		
		\App\Models\PizzaComponent::removeExisting($validation['id']);
	}
    
    public function currencyData()
    {
		echo json_encode(
			\App\Models\PizzaComponentCurrency::getFrontendData()
		);
	}
	
	public function saveCurrency(Request $request)
	{
		$validation = $request->validate([
			'id' => 'required|numeric',
			'symbol' => 'required|string|regex:/^[A-Z]{3}/',
			'displayName' => 'required|string|max:64',
			'decimals' => 'required|numeric|between:0,8'
		]);
		
		$id = null;
		if($validation['id'] < 0) {
			$id = \App\Models\PizzaComponentCurrency::insertNew(
				$validation
			);
		}
		else {
			$id = \App\Models\PizzaComponentCurrency::updateExisting(
				$validation
			);
		}
		
		echo json_encode((object)[
			'id' => $id,
		]);
	}
	
	public function removeCurrency(Request $request)
	{
		$validation = $request->validate([
			'id' => 'required|exists:pizza_component_currencies,id'
		]);
		
		\App\Models\PizzaComponentCurrency::removeExisting(
			$validation['id']
		);
	}
}
