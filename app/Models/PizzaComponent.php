<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PizzaComponent
 *
 * @property int $id ID
 * @property string $name Name / Title of the pizza component
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponent query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponent whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponent whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PizzaComponent extends Model
{
	protected $table = 'pizza_components';
	
	public static function getFrontendData() {
		$all = self::all();
		$data = [];
		
		foreach($all as $item) {
			$data[] = (object)[
				'id' => $item->id,
				'name' => $item->name,
				'price' => PizzaComponentPrice::getFrontendData(
					$item->id
				),
			];
		}
		
		return $data;
	}
	
	public static function insertNew(array $data)
	{
		$component = new self();
		$id = $component->saveComponentData($data);
		$component->generatePricesForNewComponent(
			json_decode($data['price'])
		);
		
		return $id;
	}
	
	public static function updateExisting(array $data)
	{
		$component = self::whereId(
			$data['id']
		)->firstOrFail();
		
		$component->updatePricesForComponent(
			json_decode($data['price'])
		);
		
		return $component->saveComponentData($data);
	}
	
	public static function removeExisting(int $id)
	{
		self::whereId($id)->delete();
			
		\App\Models\PizzaComponentPrice
			::wherePizzaComponentId($id)
			->delete();
	}
	
	private function saveComponentData(array $data)
	{
		$this->name = $data['name'];
		$this->save();
		
		return $this->id;
	}
	
	private function generatePricesForNewComponent(array $componentPrice) {
		$currencies = \App\Models\PizzaComponentCurrency::all();
		foreach($currencies as $currency) {
			foreach($componentPrice as $cPrice) {
				if($currency->symbol == $cPrice->symbol) {
					$price = new \App\Models\PizzaComponentPrice();
					$price->value = $cPrice->value;
					$price->pizza_component_id = $this->id;
					$price->pizza_component_currency_id = $currency->id;
					$price->save();
				}
			}
		}
	}
	
	private function updatePricesForComponent(array $componentPrice) {
		$currencies = \App\Models\PizzaComponentCurrency::all();
		foreach($currencies as $currency) {
			foreach($componentPrice as $cPrice) {
				if($currency->symbol == $cPrice->symbol) {
					$price = \App\Models\PizzaComponentPrice::where([
						['pizza_component_id', '=', $this->id],
						['pizza_component_currency_id', '=', $currency->id]
					])->firstOrFail();
					
					$price->value = $cPrice->value;
					$price->save();
				}
			}
		}
	}
}
