<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PizzaComponentCurrency
 *
 * @property int $id ID
 * @property string $symbol 3 letter symbol or the currency
 * @property string $display_name Displayed name of the currency
 * @property int $decimals Number of decimals of the price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponentCurrency newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponentCurrency newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponentCurrency query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponentCurrency whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponentCurrency whereDecimals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponentCurrency whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponentCurrency whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponentCurrency whereSymbol($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponentCurrency whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PizzaComponentCurrency extends Model
{
    protected $table = 'pizza_component_currencies';
    
    public static function getFrontendData() {
		$all = self::all();
		$data = [];
		
		foreach($all as $item) {
			$data[] = (object)[
				'id' => $item->id,
				'symbol' => $item->symbol,
				'displayName' => $item->display_name,
				'decimals' => $item->decimals,
			];
		}
		
		return $data;
	}
	
	public static function insertNew(array $data)
	{
		$currency = new self();
		$id = $currency->saveCurrencyData($data);
		$currency->generatePricesForNewCurrency();
		
		return $id;
	}
	
	public static function updateExisting(array $data)
	{
		$currency = self::whereId(
			$data['id']
		)->firstOrFail();
		
		return $currency->saveCurrencyData($data);
	}
	
	public static function removeExisting(int $id)
	{
		self::whereId($id)
			->delete();
		
		\App\Models\PizzaComponentPrice
			::wherePizzaComponentCurrencyId($id)
			->delete();
	}
	
	private function saveCurrencyData(array $data)
	{
		$this->symbol = $data['symbol'];
		$this->display_name = $data['displayName'];
		$this->decimals = $data['decimals'];
		
		$this->save();
		
		return $this->id;
	}
	
	private function generatePricesForNewCurrency() {
		$components = \App\Models\PizzaComponent::all();
		foreach($components as $component) {
			$price = new \App\Models\PizzaComponentPrice();
			$price->value = 0;
			$price->pizza_component_id = $component->id;
			$price->pizza_component_currency_id = $this->id;
			$price->save();
		}
	}
}
