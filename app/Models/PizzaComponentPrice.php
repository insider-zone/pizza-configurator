<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PizzaComponentPrice
 *
 * @property int $id ID
 * @property float $value Value of the pizza component price
 * @property int $pizza_component_id ID of the belonging pizza component
 * @property int $pizza_component_currency_id ID of the belonging pizza component currency
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponentPrice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponentPrice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponentPrice query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponentPrice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponentPrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponentPrice wherePizzaComponentCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponentPrice wherePizzaComponentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponentPrice whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PizzaComponentPrice whereValue($value)
 * @mixin \Eloquent
 */
class PizzaComponentPrice extends Model
{
    protected $table = 'pizza_component_prices';
    
    public static function getFrontendData(int $pizzaComponentId) {
		$prices = self::wherePizzaComponentId($pizzaComponentId)->get();
		$data = [];
		
		foreach($prices as $price) {
			$currency = PizzaComponentCurrency::whereId(
				$price->pizza_component_currency_id
			)->firstOrFail();
			
			$data[] = (object)[
				'symbol' => $currency->symbol,
				'value' => round($price->value, $currency->decimals),
			];
		}
		
		return $data;
	}
}
