<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'ConfiguratorController@index')->name('home');
Route::get('/data', 'ConfiguratorController@data')->name('data');

Auth::routes();
Route::get('/logout', 'LogoutController@logout')->name('logout');

Route::redirect('/admin', '/admin/components');
Route::group(['prefix' => 'admin'], function(){
	Route::get('components', 'AdminController@showComponentEditor')
		->name('componentEditor');
	Route::get('currencies', 'AdminController@showCurrencyEditor')
		->name('currencyEditor');
		
	Route::get('components/data', 'AdminController@componentData');
	Route::get('currencies/data', 'AdminController@currencyData');
		
	Route::post('components/save', 'AdminController@saveComponent');
	Route::post('currencies/save', 'AdminController@saveCurrency');
		
	Route::post('components/remove', 'AdminController@removeComponent');
	Route::post('currencies/remove', 'AdminController@removeCurrency');
});

