@extends('layouts.backend')

@section('app-id', 'component-editor-app')

@section('content')
<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
	<p>Admin / <strong>pizza component</strong> editor</p>
	<hr>
</div>
<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
	<component-block v-for="component in components" v-bind:key="component.id" v-bind:component="component"></component-block>
</div>
<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"><button v-on:click="addComponent()" class="btn btn-primary btn-block btn-lg" type="button">Add new component</button>
	<div class="row">
		<div class="col">
			<hr>
		</div>
	</div>
</div>
@endsection

@section('vue-script')
<script src="/assets/js/admin/component-editor.vue.js"></script>
@endsection
