@extends('layouts.backend')

@section('app-id', 'currency-editor-app')

@section('content')
<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
	<p>Admin / <strong>currency</strong> editor</p>
	<hr>
</div>
<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
	<currency-block v-for="currency in currencies" v-bind:currency="currency" v-bind:key="currency.id"></currency-block>
</div>
<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"><button v-on:click="addCurrencyComponent()" class="btn btn-primary btn-block btn-lg" type="button">Add new currency</button>
	<div class="row">
		<div class="col">
			<hr>
		</div>
	</div>
</div>
@endsection

@section('vue-script')
<script src="/assets/js/admin/currency-editor.vue.js"></script>
@endsection
