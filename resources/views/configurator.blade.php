@extends('layouts.frontend')

@section('content')
<div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
	<h4>Choose your toppings! </h4>
	<p>Available toppings:</p>
	<div id="available-components">
		<available-topping v-for="topping in availableToppings" v-bind:topping="topping" v-bind:currency="selectedCurrencySymbol" v-bind:key="topping.id"></available-topping>
	</div>
</div>
<div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
	<div class="row">
		<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<h4>
				Your pizza: 
				<span class="text-success price" v-if="isMinimumToppingCountReached">@{{Math.round(totalValue[selectedCurrencySymbol] * roundFactor) / roundFactor}}</span>
				<span class="text-success price" v-else>?</span>
				<currency-selector v-bind:currencies="currencies" v-bind:selected="selectedCurrencySymbol"></currency-selector>
			</h4>
			<p v-if="isMinimumToppingCountReached">The dream <strong>pizza</strong> you created:</p>
			<p v-else>You need to select at least @{{minimumToppingCount}} toppings to calculate price!</p>
			<div id="selected-components">
				<selected-topping v-for="topping in selectedToppings" v-bind:topping="topping" v-bind:currency="selectedCurrencySymbol" v-bind:key="topping.id"></selected-topping>
			</div>
		</div>
	</div>
</div>
@endsection
