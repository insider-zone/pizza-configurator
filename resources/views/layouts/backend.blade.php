<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Pizza Configurator Admin</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/adminstyles.css">
</head>

<body>
	<div id="loader"></div>
    <div class="container" id="@yield('app-id')">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <ul class="nav nav-pills">
                    <li class="nav-item"><a class="nav-link active" href="{{route('componentEditor')}}">Components</a></li>
                    <li class="nav-item"><a class="nav-link active" href="{{route('currencyEditor')}}">Currencies</a></li>
                    <li class="nav-item logout"><a class="nav-link active text-white-50" href="{{route('logout')}}">Logout</a></li>
                </ul>
            </div>
            @yield('content')
        </div>
    </div>
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.*/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>
    @yield('vue-script')
</body>

</html>
